# Dockerizing Django


Based on:

* [Dockerizing Django with Gunicorn and Nginx](https://testdriven.io/blog/dockerizing-django-with-postgres-gunicorn-and-nginx/ "Dockerizing")
* [Docker and Gitlab-CI](https://medium.com/@jwdobken/python-django-with-docker-and-gitlab-ci-b83cc4e7e2e "Docker and Gitlab-CI")
* [Modern devops django](https://peakwinter.net/blog/modern-devops-django/ "Modern DevOps Django")

